from django.db import models

# Create your models here.
class Article(models.Model):
    title=models.CharField(max_length=100)
    detail=models.TextField(default='')
    pulldown = models.CharField(max_length=100)
    pulldown1 = models.CharField(max_length=100)
    pulldown2 = models.CharField(max_length=100)
    pulldown3= models.CharField(max_length=100)
    pulldown4 = models.CharField(max_length=100)
    pulldown5 = models.CharField(max_length=100)
    price=models.CharField(max_length=100)
